package com.lutukai.pos.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.lutukai.pos.R
import com.lutukai.pos.activity.AssetManagement
import com.lutukai.pos.activity.CustomerGroup
import com.lutukai.pos.retrofit.ApiClient
import com.lutukai.pos.utils.getToken
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException


class HomeFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        PushDownAnim.setPushDownAnimTo(contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                startActivity(Intent(context, CustomerGroup::class.java))
            }
        PushDownAnim.setPushDownAnimTo(asset).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                startActivity(Intent(context, AssetManagement::class.java))
            }
//        PushDownAnim.setPushDownAnimTo(test).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
//            .setOnClickListener {
////                startActivity(Intent(context, CaptureBeacons::class.java))
//            }
        getData()
    }

    private fun getData() {
        GlobalScope.launch(Dispatchers.Main) { getUsersData()  }
    }

    private suspend fun getUsersData() {
        try {
            val response = ApiClient.webService.getUserData(context!!.getToken())
            if (response.isSuccessful){
//                username.text = response.body()!!.user.username
                company.text = response.body()!!.user.first_name
            }
        }catch (e:IOException){

        }
    }

}