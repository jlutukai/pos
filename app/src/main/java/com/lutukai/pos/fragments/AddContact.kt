package com.lutukai.pos.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.lutukai.pos.R
import com.lutukai.pos.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.add_new_contact_bottomsheet.*

class AddContact : BottomSheetDialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.add_new_contact_bottomsheet,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            val nameC : String = name.text.toString().trim()
            val email : String = mail.text.toString().trim()
            val phoneC : String = phone.text.toString().trim()
            val biz : String = business_name.text.toString().trim()

            PushDownAnim.setPushDownAnimTo(add_contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
                when {
                    nameC.isEmpty() -> name.error = "Required"
                    email.isEmpty() -> mail.error ="Required"
                    phoneC.isEmpty() -> phone.error = "Required"
                    else -> addContact()
                }
            }

    }

    private fun addContact() {

    }
}