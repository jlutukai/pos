package com.lutukai.pos.model

data class AssetTranfers(
    val movements : List<Movements>,
    val movement_status : List<String>,
    val asset_movement_status : List<String>
)