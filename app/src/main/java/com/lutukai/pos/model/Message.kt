package com.lutukai.pos.model

data class Message(
    val error : String,
    val message : String
)