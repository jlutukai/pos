package com.lutukai.pos.model

data class Units(
    val id : Int,
    val actual_name: String,
    val short_name : String
)