package com.lutukai.pos.model

data class UserData(
    val user : User,
    val contact : Contact,
    val status_code : Int
)