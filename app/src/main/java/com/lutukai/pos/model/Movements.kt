package com.lutukai.pos.model

data class Movements(
    val id : Int,
    val tracking_number : String,
    val reference : String,
    val movement_date : String,
    val from_station_id : Int?,
    val from_contact_id : Int?,
    val to_station_id : Int?,
    val to_contact_id : Int?,
    val status : Int,
    val vehicle_id : Int,
    val assets : List<Assetss>,
    val from_station : AllStations?,
    val to_station : AllStations?,
    val from_contact : Contact?,
    val to_contact : Contact?

)