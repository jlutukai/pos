package com.lutukai.pos.model

data class TransferReasons(
    val id : Int,
    val reason : String
)
