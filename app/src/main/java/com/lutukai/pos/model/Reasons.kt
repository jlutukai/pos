package com.lutukai.pos.model

data class Reasons(
    val transfer_reasons : List<TransferReasons>
)