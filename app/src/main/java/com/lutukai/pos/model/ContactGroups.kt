package com.lutukai.pos.model

data class ContactGroups(
    val contact_groups : List<ContactGroup>
)