package com.lutukai.pos.model

data class Token(
    val token: String
)