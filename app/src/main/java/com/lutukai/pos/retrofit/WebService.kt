package com.lutukai.pos.retrofit

import com.lutukai.pos.model.*
import retrofit2.Response
import retrofit2.http.*

interface WebService {
    @POST("login/")
    suspend fun loginUser(@Query("username") username: String, @Query("password") password: String): Response<Token>
    @GET("user")
    suspend fun getUserData(@Header("authorization") authorization: String) : Response<UserData>
    @POST("contact/create")
    suspend fun addContact(
        @Header("authorization") authorization: String,
        @Query("name") name: String,
        @Query("email") email: String,
        @Query("mobile") phone: String,
        @Query("customer_group_id") id: String,
        @Query("supplier_business_name") biz: String
    ): Response<ContactObj>

    @POST("contact/update/{id}")
    suspend fun updateContact(
        @Header("authorization") authorization: String,
        @Path("id") id: String,
        @Query("name") name: String,
        @Query("email") email: String?,
        @Query("mobile") phone: String?,
        @Query("supplier_business_name") biz: String?
    ) : Response<ContactObj>

//    @Multipart
    @POST("batch/create")
    suspend fun addBatch(
        @Header("authorization") authorization: String,
        @Query("description") desc: String?,
        @Query("batch_no") bno: String?,
        @Query("asset_type_id") assetid: String,
        @Query("unit_id") unitid: String,
        @Query("contact_id") contactid: String,
        @Query("has_warranty") warranty: Int,
        @Query("warranty_duration") duration: String,
        @Query("warranty_interval") interval: String,
        @Query("received_at") stationid: String,
        @Query("longitude") long : String,
        @Query("latitude") lat : String
//        @Query("description") desc : String,
//        @Part("image") image : File
    ): Response<AllBatches>

    @POST("batch/add_assets/{id}")
    suspend fun addAsset(@Header("authorization") authorization: String,
                         @Path("id") id: String,
                         @Query("received_at") sid : String,
                         @Query("initial_value") value : String,
                         @Query("serials") serial: String): Response<Assetss>

    @POST("asset_transfer")
    suspend fun addMovement(@Header("authorization") authorization: String,
                            @Query("from_station_id") fsid: String?,
                            @Query("to_station_id") tsid: String?,
                            @Query("created_by") uid : String,
                            @Query("from_contact_id") fuid : String?,
                            @Query("to_contact_id") tuid : String?,
                            @Query("reason") reasons: String,
                            @Query("reference") ref : String,
                            @Query("longitude") long : String,
                            @Query("latitude") lat : String
                            ) : Response<Movement>
    @POST("asset_transfer/change_status")
    suspend fun changeStatus(@Header("authorization") authorization: String,
                             @Query("movement_id") mid : String,
                             @Query("asset_id") aid : String,
                             @Query("latitude") lat : String,
                             @Query("longitude") long : String,
                             @Query("status") status : String) : Response<Void>

    @POST("asset_transfer/add_assets")
    suspend fun addAssetsToMovement(@Header("authorization") authorization: String,
                                    @Query("movement_id") mid : Int,
                                    @Query("serials") serial: String):Response<Void>//experiment - should return code  --

    @GET("asset_transfers")
    suspend fun fetchAssetTransfers(@Header("authorization") authorization: String) : Response<AssetTranfers>

    @GET("asset_transfer_reasons")
    suspend fun fetchReasons(@Header("authorization") authorization: String) : Response<Reasons>
    @GET("asset_types")
    suspend fun fetchAssets(@Header("authorization") authorization: String): Response<AllAssets>

    @GET("contact_groups")
    suspend fun fetchContactGroups(@Header("authorization") authorization: String): Response<ContactGroups>

    @GET("contacts")
    suspend fun fetchContacts(@Header("authorization") authorization: String): Response<Contacts>

    @GET("categories")
    suspend fun fetchCategories(@Header("authorization") authorization: String): Response<Categories>

    @GET("product/{id}")
    suspend fun fetchProducts(@Header("authorization") authorization: String, @Path("id") id: Int): Response<Products>

}