package com.lutukai.pos.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.lutukai.pos.R
import com.lutukai.pos.adapter.AssetAdapter
import com.lutukai.pos.model.Assetss
import com.lutukai.pos.retrofit.ApiClient
import com.lutukai.pos.utils.*
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_assets.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class Assets : AppCompatActivity() {
    private lateinit var assetList: MutableList<Assetss>
    private lateinit var assets: List<Assetss>
    private lateinit var serials : String
    private lateinit var rfids : String
    private  var stationid: String = ""
    private lateinit var values : String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assets)
        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)

        val id: String? = intent.getStringExtra("id")
        val sid : String? = intent.getStringExtra("sid")
        setId(id!!)
        setSid(sid!!)
        rv_assets.layoutManager = LinearLayoutManager(this)
        rv_assets.isNestedScrollingEnabled = false
        getLists()

        PushDownAnim.setPushDownAnimTo(add_asset).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
//            showToast("$id $sid")
            values = id
            serials = sid
            startActivity(Intent(this, CaptureBeacons::class.java)
                .putExtra("type" , "addtobatch")
                .putExtra("stid", stationid))
        }
    }



    private fun getLists() {
        if (isNetworkConnected(this)) {
            GlobalScope.launch(Dispatchers.Main) { lists() }
        } else {
            progress.visibility = View.GONE
            showToast("Check Internet Connection")
        }
    }

    private suspend fun lists() {
        val id: String? = intent.getStringExtra("id")
        try {
            val response = ApiClient.webService.fetchAssets(getToken())
            if (response.isSuccessful) {
                assets = response.body()!!.assets
                assetList = arrayListOf()
                for (i in assets) {
                    if (i.batch_id == id!!.toInt()) {
                        assetList.add(i)
                        stationid = i.current_station_id.toString()
                    }
                }
                if (assetList.size == 0){
                    showToast("No Assets Available currently...")
                }
                assetList.let {
                    progress.visibility = View.GONE
                    val assetAdapter = AssetAdapter(this, assetList)
                    rv_assets.adapter = assetAdapter
                }
            }

        } catch (e: IOException) {

        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
