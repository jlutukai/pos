package com.lutukai.pos.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.lutukai.pos.R
import com.lutukai.pos.adapter.BatchesAdapter
import com.lutukai.pos.model.AllBatches
import com.lutukai.pos.retrofit.ApiClient
import com.lutukai.pos.utils.getToken
import com.lutukai.pos.utils.isNetworkConnected
import com.lutukai.pos.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_batches.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class Batches : AppCompatActivity() {
    private lateinit var batchList : MutableList<AllBatches>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_batches)
        setSupportActionBar(toolbar_b)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        val id: String? = intent.getStringExtra("id")
        rv_batches.layoutManager = LinearLayoutManager(this)
        rv_batches.isNestedScrollingEnabled = false
        getBatchTypes()
        PushDownAnim.setPushDownAnimTo(add_batch).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            startActivity(Intent(this, AddBatch::class.java).putExtra("id",id))
        }
    }

    private fun getBatchTypes() {
        if (isNetworkConnected(this)){
//            generalViewModel.getUpdatedAssetTypes()
            GlobalScope.launch(Dispatchers.Main) { batches()  }
        }else{
            showToast("Check Internet Connection")
        }
    }

    private suspend fun batches() {
        val id: String? = intent.getStringExtra("id")
        try {
            val response = ApiClient.webService.fetchAssets(getToken())
            if (response.isSuccessful){
                val allbatches = response.body()!!.batches
                batchList = arrayListOf()
                for (i in allbatches){
                    if (i.asset_type_id == id!!.toInt()){
                        batchList.add(i)
                    }
                }
                batchList.let {
                    progress.visibility = View.GONE
                    val batchesAdapter = BatchesAdapter(this, batchList)
                    rv_batches.adapter = batchesAdapter
                }
            }
        }catch (e: IOException){

        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
