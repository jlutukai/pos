package com.lutukai.pos.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.l4digital.fastscroll.FastScrollRecyclerView
import com.lutukai.pos.adapter.ContactAdapter
import com.lutukai.pos.model.Contact
import com.lutukai.pos.room.viewModel.GeneralViewModel
import com.lutukai.pos.utils.isNetworkConnected
import com.lutukai.pos.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_contacts.*


class Contacts : AppCompatActivity() {
    private lateinit var generalViewModel: GeneralViewModel
    private lateinit var contactList: MutableList<Contact>
    private lateinit var recyclerView: FastScrollRecyclerView
    @SuppressLint("DefaultLocale")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.lutukai.pos.R.layout.activity_contacts)
        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)

        val id: String? = intent.getStringExtra("id")

        recyclerView = findViewById(com.lutukai.pos.R.id.rv_contacts)

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.isNestedScrollingEnabled = false




        generalViewModel = ViewModelProviders.of(this).get(GeneralViewModel::class.java)
        getContacts(id!!)
//        var bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)

        PushDownAnim.setPushDownAnimTo(add).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                //            val addContact = AddContact()
//            addContact.show(supportFragmentManager,addContact.tag)
                startActivity(
                    Intent(this@Contacts, AddContact::class.java)
                        .putExtra("id", id)
                )
            }
    }


    @SuppressLint("DefaultLocale")
    private fun getContacts(id: String) {
        if (isNetworkConnected(this)) {
            generalViewModel.getUpdatedContacts()
        } else {
            showToast("From Room")
        }
        generalViewModel.getLocalContacts().observe(this@Contacts, Observer { contacts ->
            if (contacts.isNotEmpty()) {
                contactList = arrayListOf()
                progress.visibility = View.GONE
                for (n in contacts) {
                    if (n.customer_group_id == id.toInt()) {
                        contactList.add(n)
                    }
                    if (n.customer_group_id == null && id.toInt() == 900) {
                        contactList.add(n)
                    }
                }
                contactList.sortWith(Comparator { lhs, rhs -> lhs.name.toLowerCase().compareTo(rhs.name.toLowerCase()) })
                val contactAdapter = ContactAdapter(this, contactList)
                recyclerView.adapter = contactAdapter
            }

        })
    }
    fun showDetails(name: String) {
        showToast(name)
//        val dialog = Dialog(this)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setCancelable(true)
//        dialog.setContentView(R.layout.contact_dialog)
//        val body : TextView = dialog.findViewById(R.id.name_contact)
//        body.text = name
//        dialog.show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
