package com.lutukai.pos.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.lutukai.pos.R
import com.lutukai.pos.adapter.ProductsAdapter
import com.lutukai.pos.model.SalesLocal
import com.lutukai.pos.room.viewModel.GeneralViewModel
import com.lutukai.pos.utils.SpacingItemDecoration
import com.lutukai.pos.utils.dpToPx
import com.lutukai.pos.utils.isNetworkConnected
import com.lutukai.pos.utils.showToast
import kotlinx.android.synthetic.main.activity_products.*

class ProductsActivity : AppCompatActivity() {
    private lateinit var generalViewModel: GeneralViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)

        toolbar.setNavigationOnClickListener { finish() }

        rv_products.layoutManager = GridLayoutManager(this, 3)
        rv_products.addItemDecoration(SpacingItemDecoration(3, dpToPx(this, 8), true))
        rv_products.setHasFixedSize(true)
        rv_products.isNestedScrollingEnabled = false

        generalViewModel = ViewModelProviders.of(this).get(GeneralViewModel::class.java)
        getProducts()
    }

    private fun getProducts() {
        if (isNetworkConnected(this)) {
            generalViewModel.getUpdatedProducts()
        } else {
            showToast("Showing cached Products")
        }

        generalViewModel.getLocalProducts().observe(this, Observer { products ->
            if (products.isNotEmpty()) {
                progress.visibility = View.GONE
                val productsAdapter = ProductsAdapter(this, products)
                rv_products.adapter = productsAdapter
                productsAdapter.onItemClick = {
                    val salesLocal =
                        SalesLocal(it.id, it.name, it.default_purchase_price, it.default_sell_price, 1)
                   generalViewModel.roomItems(salesLocal)
                }
            }
        })

    }
}
