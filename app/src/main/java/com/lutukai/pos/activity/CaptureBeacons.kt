package com.lutukai.pos.activity

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.Spinner
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.lutukai.pos.R
import com.lutukai.pos.adapter.BeaconsAdapter
import com.lutukai.pos.ble.BTLEDevice
import com.lutukai.pos.ble.BroadcastReceiverBTState
import com.lutukai.pos.ble.Scanner
import com.lutukai.pos.ble.Utils
import com.lutukai.pos.model.AllStations
import com.lutukai.pos.model.Assetss
import com.lutukai.pos.model.Message
import com.lutukai.pos.retrofit.ApiClient
import com.lutukai.pos.utils.getToken
import com.lutukai.pos.utils.getid
import com.lutukai.pos.utils.getsid
import com.lutukai.pos.utils.showToast
import com.neovisionaries.bluetooth.ble.advertising.ADPayloadParser
import com.neovisionaries.bluetooth.ble.advertising.IBeacon
import kotlinx.android.synthetic.main.activity_capture_beacons.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class CaptureBeacons : AppCompatActivity(), AdapterView.OnItemClickListener, View.OnClickListener {

    @kotlin.jvm.JvmField
    val REQUEST_ENABLE_BT = 1
    val BTLE_SERVICES = 2
    private val TAG = CaptureBeacons::class.java.simpleName
    private var mBTDevicesHashMap: HashMap<String, BTLEDevice>? = null
    private var mBTDevicesArrayList: ArrayList<BTLEDevice> = arrayListOf()
    private var mBTDevices: ArrayList<BTLEDevice> = arrayListOf()
    private var assetsBT: ArrayList<BTLEDevice> = arrayListOf()
    private val macs = ArrayList<String>()
    private val proxy = ArrayList<String>()
    private var adapter: BeaconsAdapter? = null
    private var spinner: Spinner? = null
    private var btn_Scan: Button? = null
    private var mBTStateUpdateReceiver: BroadcastReceiverBTState? = null
    private var mBTLeScanner: Scanner? = null
    private lateinit var stations : List<AllStations>
    private lateinit var assets : List<Assetss>
    private  var allassets : ArrayList<String> = arrayListOf()
    private  var allassetsins : ArrayList<String> = arrayListOf()
    private val selectedMacs : ArrayList<String> = ArrayList()
    private  var userstation : String = ""
    private  var contactID: String = ""

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_capture_beacons)

        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)

        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Utils.toast(applicationContext, "BLE Not Supported")

        }

        proxy.add("Select none")
        proxy.add("Select all")
//
        val type: String? = intent.getStringExtra("type")
        val iid : String? = intent.getStringExtra("id")


        if (iid.isNullOrEmpty()){
            showToast("null")
        }
        mBTStateUpdateReceiver = BroadcastReceiverBTState(applicationContext)
        mBTLeScanner = Scanner(this, 90000000, -105)

        mBTDevicesHashMap = HashMap()
        mBTDevicesArrayList = ArrayList()
        mBTDevices = ArrayList()

        if (type == "addtobatch") {
            adapter = BeaconsAdapter(this, assetsBT)
            rv_beacons.layoutManager = LinearLayoutManager(this)
            rv_beacons.isNestedScrollingEnabled = false
            rv_beacons.adapter = adapter
            count.text =  "Scanned : "+mBTDevicesArrayList.size.toString()+" Available : "+assetsBT.size.toString()

        }
        if (type == "addToMovement"){
            adapter = BeaconsAdapter(this, mBTDevices)
            rv_beacons.layoutManager = LinearLayoutManager(this)
            rv_beacons.isNestedScrollingEnabled = false
            rv_beacons.adapter = adapter
            count.text = "Scanned : "+mBTDevicesArrayList.size.toString()+" Available : "+mBTDevices.size.toString()
        }

        btn_Scan = findViewById(R.id.btn_scan)
        this.btn_Scan!!.setOnClickListener(this)

        cap.setOnClickListener {
            cap.text = "Finish"
            mBTLeScanner!!.stop()
            stopScan()
            if (type == "addtobatch") {
                val id: String = getid()
                val sid: String = getsid()
                addtobatch(id, sid)
            }
            if (type == "addToMovement"){
                addtomovement(iid!!)
            }

        }



    }



    private fun sortList() {
        GlobalScope.launch(Dispatchers.Main) { checklist() }
    }

    private suspend fun checklist() {
        val type: String? = intent.getStringExtra("type")
        val stid : String? = intent.getStringExtra("stid")
        try {
            val response = ApiClient.webService.fetchAssets(getToken())
            if (response.isSuccessful){
                stations = response.body()!!.stations
                assets = response.body()!!.assets


            }
        }catch (e:IOException){

        }
        try {
            val response = ApiClient.webService.getUserData(getToken())
            if (response.isSuccessful){
                if (response.body()!!.user.is_customer == 1) {
                    stations = response.body()!!.user.stations
                    val uid =response.body()!!.user.id
                    for (i in stations){
                        if (i.id.toString() == stid!! ){
                            userstation =i.pivot.business_location_id.toString()
                        }else{
                            contactID = response.body()!!.contact.id.toString()
                        }
                    }
                }
            }
        }catch (e:IOException){
            //error
        }
        allassets.clear()
        for (i in assets){
            allassetsins.add(i.serial)
            if (i.current_station_id.toString() == stid && i.in_company == 1 ){
                allassets.add(i.serial)
            }
            else if (i.current_contact_id.toString() == contactID  ){
                allassets.add(i.serial)
            }
        }
        if (type == "addtobatch"){
        for (i in mBTDevicesArrayList){
            if (!allassetsins.contains(i.address)){
                assetsBT.add(i)
            }else if (allassetsins.isEmpty()){
                assetsBT.add(i)
            }
        }
            adapter = BeaconsAdapter(this, assetsBT)
            rv_beacons.adapter = adapter
            adapter!!.notifyDataSetChanged()
            count.text =  "Scanned : "+mBTDevicesArrayList.size.toString()+" Available : "+assetsBT.size.toString()

        }
        if (type == "addToMovement") {

            mBTDevices.clear()
            for (i in mBTDevicesArrayList) {
                if (allassets.contains(i.address)) {
                    mBTDevices.add(i)
                }else if (allassets.isEmpty()){
                   showToast("No Asset Available for the move")
                }
            }
            adapter = BeaconsAdapter(this, mBTDevices)
            rv_beacons.adapter = adapter
            adapter!!.notifyDataSetChanged()
            count.text = "Scanned : "+mBTDevicesArrayList.size.toString()+" Available : "+mBTDevices.size.toString()
            return
        }

    }


    private fun addtomovement(iid: String) {
        GlobalScope.launch(Dispatchers.Main) { addAssetsToMov(iid) }

    }

    private suspend fun addAssetsToMov(iid: String) {
        selectedMacs.clear()
        for (i in mBTDevices){
            if (i.checked == true){
                selectedMacs.add(i.address)
            }
        }

        if (selectedMacs.isEmpty()) {
            showToast("None Selected")
        }else{

        try {
            val response = ApiClient.webService.addAssetsToMovement(getToken(),iid.toInt(),selectedMacs.joinToString(","))
            if (response.code() == 200){
                startActivity(Intent(this, AssetManagement::class.java))
                finish()
            }
        }catch (e:IOException){
//            e.message?.let { showToast(it) }
        }
        }

    }

    private fun addtobatch(id: String, sid: String) {
        GlobalScope.launch(Dispatchers.Main) { addAssetsToBa(id, sid) }
    }

    private suspend fun addAssetsToBa(id: String, sid: String) {
        val initial = "1000"
        selectedMacs.clear()
        for (i in mBTDevicesArrayList){
            if (i.checked == true){
                selectedMacs.add(i.address)
            }
        }
        if (selectedMacs.isEmpty()) {
            showToast("None Selected" )
        }else {
            try {
                val response = ApiClient.webService.addAsset(
                    getToken(),
                    id,
                    sid,
                    initial,
                    selectedMacs.joinToString(",")
                )
                if (response.code() == 200) {
                    startActivity(Intent(this, Assets::class.java).putExtra("id", id).putExtra("sid", sid))
                    finish()
                    Log.d("bjdsbjdbsdf", "success")
                } else {
                    val gson = Gson()
                    val errorResponse = gson.fromJson<Message>(
                        response.errorBody()!!.charStream(),
                        Message::class.java
                    )
                    val msg = errorResponse.message
                    showToast(msg)
                }
            } catch (e: IOException) {
                Log.d("Add Macs : ", "error --- " + e.message)
            }
        }
    }

    private fun setListView(item: Any) {
//        val prox = item.toString()
//        if (prox == "Select all") {
//            for (i in mBTDevicesArrayList!!) {
//                i.checked = true
//            }
//            for (i in mBTDevices!!) {
//                i.checked = true
//            }
//            adapter!!.notifyDataSetChanged()
//        }


    }

    override fun onStart() {
        super.onStart()

        registerReceiver(
            mBTStateUpdateReceiver,
            IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        )
    }

    override fun onPause() {
        super.onPause()

        //        unregisterReceiver(mBTStateUpdateReceiver);
        stopScan()
    }

    override fun onStop() {
        super.onStop()

        unregisterReceiver(mBTStateUpdateReceiver)
        stopScan()
    }

    public override fun onDestroy() {
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        // Check which request we're responding to
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ENABLE_BT) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                //                Utils.toast(getApplicationContext(), "Thank you for turning on Bluetooth");
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Utils.toast(applicationContext, "Please turn on Bluetooth")
            }
        } else if (requestCode == BTLE_SERVICES) {
            // Do something
        }
    }

    override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        val context = view.context


        //        Utils.toast(context, "List Item clicked");
        // do something with the text views and start the next activity.

        stopScan()


        val name = mBTDevicesArrayList.get(position).name
        val address = mBTDevicesArrayList.get(position).address

        //        Intent intent = new Intent(this, Activity_BTLE_Services.class);
        //        intent.putExtra(Activity_BTLE_Services.EXTRA_NAME, name);
        //        intent.putExtra(Activity_BTLE_Services.EXTRA_ADDRESS, address);
        //        startActivityForResult(intent, BTLE_SERVICES);
    }

    override fun onClick(v: View) {

        when (v.id) {

            R.id.btn_scan -> {
                Utils.toast(applicationContext, "Scan Button Pressed")

                if (!mBTLeScanner!!.isScanning) {

                    GlobalScope.launch(Dispatchers.Main) { startScan() }
                    checkSpinner()
                } else {
                    GlobalScope.launch(Dispatchers.Main) { stopScan() }
                }
            }
            else -> {
            }
        }

    }

    private fun checkSpinner() {
//        Spinner(this)
//        val stringArrayAdapter = ArrayAdapter(this, R.layout.spinner_item, proxy)
//        stringArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
//        spinner = findViewById(R.id.proximity)
//        spinner!!.adapter = stringArrayAdapter
//        spinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
//                val item = adapterView.getItemAtPosition(i)
//                GlobalScope.launch(Dispatchers.Main) { setListView(item) }
//            }
//
//            override fun onNothingSelected(adapterView: AdapterView<*>) {
//
//            }
//        }

    }

    fun addDevice(device: BluetoothDevice, rssi: Int, scanRecord: ByteArray) {
        val type: String? = intent.getStringExtra("type")
        val structures = ADPayloadParser.getInstance().parse(scanRecord)
        for (structure in structures) {
            if (structure is IBeacon) {
                val uuid = structure.uuid
                val major = structure.major
                val minor = structure.minor
                val power = structure.power
                val address = device.address
                val dist = Utils.calculateAccuracy(power, rssi.toDouble())
                if (!mBTDevicesHashMap?.containsKey(address)!!) {
                    val btleDevice = BTLEDevice(device, structure, false, dist)
                    btleDevice.setRSSI(rssi)

                    mBTDevicesHashMap?.set(address, btleDevice)
                    mBTDevicesArrayList.add(btleDevice)
                    macs.add(address)
                    if (type == "addToMovement"){
                        sortList()
                    }
                    if (type == "addtobatch"){
                        sortList()
                    }
                } else {
                    mBTDevicesHashMap!![address]!!.setRSSI(rssi)
                }
            }
        }


        adapter!!.notifyDataSetChanged()
    }

    fun startScan() {
        btn_Scan!!.text = "Scanning..."

        mBTDevicesArrayList.clear()
        mBTDevicesHashMap!!.clear()

        mBTLeScanner!!.start()
    }

    fun stopScan() {
        btn_Scan!!.text = "Scan Again"

        mBTLeScanner!!.stop()
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}
