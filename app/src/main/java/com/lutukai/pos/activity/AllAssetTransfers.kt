package com.lutukai.pos.activity

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.lutukai.pos.R
import com.lutukai.pos.adapter.AssetTransferAdapter
import com.lutukai.pos.model.AllStations
import com.lutukai.pos.model.Contact
import com.lutukai.pos.model.Movements
import com.lutukai.pos.retrofit.ApiClient
import com.lutukai.pos.utils.getToken
import com.lutukai.pos.utils.isNetworkConnected
import com.lutukai.pos.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_all_asset_transfers.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.*


class AllAssetTransfers : AppCompatActivity() {

    var sorted : List<Movements> = listOf()
    var search  : MutableList<Movements> = arrayListOf()
    var allstations: List<AllStations> = listOf()
    var allcontacts: List<Contact> = listOf()
    var alltransfers : List<Movements> = listOf()
    var status: List<String> = listOf()
    var st1 : String = ""
    private lateinit var assetTransferAdapter : AssetTransferAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.lutukai.pos.R.layout.activity_all_asset_transfers)
        setSupportActionBar(toolbar_t)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        val id = intent.getStringExtra("id")
        val type = intent.getStringExtra("type")
        getData()
        rv_transfers.layoutManager = LinearLayoutManager(this)
        rv_transfers.isNestedScrollingEnabled = false

        PushDownAnim.setPushDownAnimTo(add_t).setScale(PushDownAnim.MODE_STATIC_DP, 8f)
            .setOnClickListener {
                if (type == "biz") {
                    startActivity(
                        Intent(this, AddAssetTransferStation::class.java)
                    )
                }
                if (type == "conta"){
                    startActivity(
                        Intent(this, AddAssetTransferContact::class.java).putExtra(
                            "id", id))
                }
            }
//
//        chip.setOnClickListener {
//            if (!chip.chipSelected){
//                chip.selectable = true
//                chip.chipSelected = true
//                showToast("selected")
//            }else if (chip.chipSelected) {
////                chip.selectable = false
////                chip.chipSelected=false
//                showToast("not selected")
//            chip1.selectable =false
//            chip1.chipSelected =false
//            chip2.selectable = false
//            chip2.chipSelected =false}
//        }
//        chip1.setOnClickListener {
//            if (!chip1.chipSelected){
//                chip1.selectable = true
//                chip1.chipSelected=true
//            }else {
//            chip.selectable =false
//            chip.chipSelected = false
//            chip2.selectable = false
//            chip2.chipSelected = false
//            }
//        }
//        chip2.setOnClickListener {
//            if (!chip2.chipSelected){
//                chip2.selectable = true
//                chip2.chipSelected=true
//            }else {
//                chip1.selectable = false
//                chip1.chipSelected = false
//                chip.selectable = false
//                chip.chipSelected = false
//            }
//        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        val stations: List<String> = listOf()
        menuInflater.inflate(R.menu.search, menu)
        val searchItem = menu!!.findItem(R.id.action_search)
        if (searchItem != null){
            val searchView = searchItem.actionView as SearchView
            val editText = searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
            editText.hint = "Search here..."

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {

                    if (newText!!.isNotEmpty()){
                        search.clear()
                        val  char = newText.toLowerCase(Locale.getDefault())
                        for (i in sorted){
//                            if(i.from_station_id != null && i.to_station_id != null )
                            if (i.reference.toLowerCase().contains(char) ||
                                status[i.status].toLowerCase().contains(char) ||
                                        i.tracking_number.toLowerCase().contains(char)
//                                ||
//                                        i.from_station!!.name.toLowerCase().isNotEmpty() && i.from_station.name.toLowerCase().contains(char) ||
//                                        i.to_station!!.name.toLowerCase().isNotEmpty() && i.to_station.name.toLowerCase().contains(char) ||
//                                        i.from_contact!!.name.toLowerCase().isNotEmpty() && i.from_contact.name.toLowerCase().contains(char) ||
//                                        i.to_contact!!.name.toLowerCase().isNotEmpty() && i.to_contact.name.toLowerCase().contains(char)
                                ){
                                search.add(i)
                            }
                        }
                        rv_transfers.adapter!!.notifyDataSetChanged()
                    }else{
                        search.clear()
                        search.addAll(sorted)
                        rv_transfers.adapter!!.notifyDataSetChanged()
                    }

                    return true
                }

            })
        }

        return super.onCreateOptionsMenu(menu)
    }

    private fun getData() {
        if (isNetworkConnected(this)) {
            GlobalScope.launch(Dispatchers.Main) { getAllData() }
        } else {
            showToast("Check Internet Connection")
        }

    }

    private suspend fun getAllData() {
        val id = intent.getStringExtra("id")
        val type = intent.getStringExtra("type")

        val transfers: MutableList<Movements> = arrayListOf()

        try {
            val response = ApiClient.webService.fetchContacts(application.getToken())
            if (response.isSuccessful) {
                allcontacts = response.body()!!.contacts
            }
        } catch (e: IOException) {
            //errors
        }
        try {
            val response = ApiClient.webService.fetchAssets(getToken())
            if (response.isSuccessful) {
                allstations = response.body()!!.stations
            }
        } catch (e: IOException) {

        }
        try {
            val response = ApiClient.webService.fetchAssetTransfers(getToken())
            if (response.isSuccessful) {
                alltransfers = response.body()!!.movements as MutableList<Movements>
                status = response.body()!!.movement_status
                progress.visibility = View.GONE
                for (i in alltransfers){
                    if (type =="conta" && i.to_contact_id.toString() == id || type =="conta" && i.from_contact_id.toString() == id  && i.assets.isNotEmpty()){
                        transfers.add(i)
                    }
                    if (i.assets.isNotEmpty() && type == "biz"){
                        transfers.add(i)
                    }
                }
                sorted  = transfers.sortedByDescending { it.tracking_number }
                search.addAll(sorted)
                assetTransferAdapter = AssetTransferAdapter(
                    this@AllAssetTransfers,
                    search,
                    allstations,
                    allcontacts,
                    status
                )
                rv_transfers.adapter = assetTransferAdapter
            }

        } catch (e: IOException) {

        }


    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

