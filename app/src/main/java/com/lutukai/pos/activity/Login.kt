package com.lutukai.pos.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.multidex.MultiDex
import com.google.gson.Gson
import com.lutukai.pos.R
import com.lutukai.pos.model.Message
import com.lutukai.pos.retrofit.ApiClient
import com.lutukai.pos.utils.showToast
import com.lutukai.pos.utils.storeToken
import com.nbbse.mobiprint3.Printer
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch
import java.io.IOException
import kotlin.coroutines.CoroutineContext

class Login : AppCompatActivity(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private lateinit var printer:Printer

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_login)
        PushDownAnim.setPushDownAnimTo(login_btn).setScale(PushDownAnim.MODE_STATIC_DP,8F).setOnClickListener {
//            showToast("clicked")
//            startActivity(Intent(this@Login, Main2Activity::class.java))
            progress_login.visibility = View.VISIBLE
            launch {
                loginUser()
            }
        }

//        printerManenos()


    }


//    private fun printerManenos() {
//        printer = Printer.getInstance()
////        print.printText("hello printer!!")
//        if ( printer.printerStatus == Printer.PRINTER_STATUS_OK) {
//            Toast.makeText(this, "Printer ready",Toast.LENGTH_LONG).show()
//            printer.printText("Welcome to my printer!!!!!")
//        }
//        when(printer.paperStatus){
//            Printer.PRINTER_NO_PAPER -> Toast.makeText(this, "NO paper",Toast.LENGTH_LONG).show()
//            Printer.PRINTER_EXIST_PAPER -> {
//                printer.printText("hello printer!!")
////                Toast.makeText(this, "paper loaded correctly",Toast.LENGTH_LONG).show()
//            }
//            Printer.PRINTER_PAPER_ERROR -> Toast.makeText(this, "paper not loaded correctly",Toast.LENGTH_LONG).show()
//        }
//        Toast.makeText(this, printer.paperStatus.toString(),Toast.LENGTH_LONG).show()
//    }
//
    private fun loginUser()  = launch{
        val name = username.text.toString().trim()
        val pass = password.text.toString().trim()

       if (name.isEmpty()){
           username.error = "REQUIRED"
           progress_login.visibility = View.GONE
           return@launch
       }
       if (pass.isEmpty()){
           password.error = "REQUIRED"
           progress_login.visibility = View.GONE
           return@launch
       }

       //No empty fields
       try {
           val response = ApiClient.webService.loginUser(name,pass)
//           val response = ApiClient.webService.posts()
           if (response.code() == 200){
               progress_login.visibility = View.GONE
                val token  = response.body()
               token?.let { storeToken("Bearer ${token.token}")
                   val intent = Intent(this@Login, Main2Activity::class.java)
               startActivity(intent)
               }
           }else {
               progress_login.visibility = View.GONE
               val gson = Gson()
               val errorResponse = gson.fromJson<Message>(response.errorBody()!!.charStream(),
                   Message::class.java)
               val msg = errorResponse.error
               showToast(msg)
           }
       }catch (e: IOException){
           progress_login.visibility = View.GONE
           showToast("Failed")
           Log.d("EXCEPTION", e.message!!)
       }
    }
//
    override fun onDestroy() {
        super.onDestroy()
        coroutineContext.cancelChildren()
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }}
//    override fun onStart() {
//        super.onStart()
////        val token:String? = getToken()
////        token?.let {
////            if (token != null){
////                startActivity(Intent(this, CategoriesActivity::class.java))
////            }
////        }
//    }
//}
