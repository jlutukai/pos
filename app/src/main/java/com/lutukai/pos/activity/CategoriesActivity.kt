package com.lutukai.pos.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lutukai.pos.R
import com.lutukai.pos.adapter.CategoriesAdapter
import com.lutukai.pos.room.viewModel.GeneralViewModel
import com.lutukai.pos.utils.SpacingItemDecoration
import com.lutukai.pos.utils.dpToPx
import com.lutukai.pos.utils.isNetworkConnected
import com.lutukai.pos.utils.showToast
import kotlinx.android.synthetic.main.activity_categories.*


class CategoriesActivity : AppCompatActivity() {
    private lateinit var generalViewModel: GeneralViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)

        rv_category.layoutManager = GridLayoutManager(this, 2)
        rv_category.addItemDecoration(SpacingItemDecoration(2, dpToPx(this, 8), true))
        rv_category.setHasFixedSize(true)
        rv_category.isNestedScrollingEnabled = false

        generalViewModel = ViewModelProviders.of(this).get(GeneralViewModel::class.java)
        getCategories()
    }

    private fun getCategories() {
        if (isNetworkConnected(this)) {
            generalViewModel.getUpdatedCategories()
        } else {
            showToast("Showing cached revenue streams")
        }

        generalViewModel.getLocalCategories().observe(this@CategoriesActivity, Observer { categories ->
            if (categories.isNotEmpty()) {
                progress.visibility = View.GONE
                val categoriesAdapter = CategoriesAdapter(this@CategoriesActivity, categories)
                rv_category.adapter = categoriesAdapter
            }else{
                showToast("empty")
            }
        })

    }


}
