package com.lutukai.pos.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.lutukai.pos.R
import com.lutukai.pos.retrofit.ApiClient
import com.lutukai.pos.utils.getToken
import com.lutukai.pos.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_add_contact.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class AddContact : AppCompatActivity() {
    private lateinit var uriContacts: Uri
    private lateinit var contactID: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_contact)

        val id: String? = intent.getStringExtra("id")

        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)

        PushDownAnim.setPushDownAnimTo(add_contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                val nameC: String = name.text.toString().trim()
                val email: String = mail.text.toString().trim()
                val phoneC: String = phone.text.toString().trim()
                val biz: String = business_name.text.toString().trim()
                when {
                    nameC.isEmpty() -> name.error = "Required"
                    email.isEmpty() -> mail.error = "Required"
                    phoneC.isEmpty() -> phone.error = "Required"
                    else -> addContact()
                }
            }
        PushDownAnim.setPushDownAnimTo(import_contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = ContactsContract.Contacts.CONTENT_TYPE
                startActivityForResult(intent, 1)
            }
    }

    @SuppressLint("Recycle")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            uriContacts = data!!.data!!
            contactPicked()
        }

    }

    @SuppressLint("Recycle")
    private fun contactPicked() {
        val projection = arrayOf(
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
        )
        val resolver = contentResolver
        val cursor = resolver.query(uriContacts, projection, null, null, null)
        cursor!!.moveToFirst()
        val indexNo = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
        val phoneNumber = getString(indexNo)
        val indexNa = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
        val nameCont = getString(indexNa)
        cursor.close()
        phone.setText(phoneNumber)
        name.setText(nameCont)

    }


    private fun addContact() {
        progress.visibility = View.VISIBLE
        GlobalScope.launch(Dispatchers.Main) { postContact() }
    }

    private suspend fun postContact() {
        val id: String? = intent.getStringExtra("id")
        val nameC: String = name.text.toString().trim()
        val email: String = mail.text.toString().trim()
        val phoneC: String = phone.text.toString().trim()
        val biz: String = business_name.text.toString().trim()
        try {
            val response =
                ApiClient.webService.addContact(getToken(), nameC, email, phoneC, id!!, biz)
            if (response.isSuccessful) showToast("success")
            progress.visibility = View.GONE
            startActivity(Intent(this@AddContact, Contacts::class.java).putExtra("id", id))
            finish()
        } catch (e: IOException) {
            //error
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
