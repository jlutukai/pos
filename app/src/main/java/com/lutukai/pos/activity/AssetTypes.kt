package com.lutukai.pos.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.lutukai.pos.R
import com.lutukai.pos.adapter.AssetTypesAdapter
import com.lutukai.pos.retrofit.ApiClient
import com.lutukai.pos.room.viewModel.GeneralViewModel
import com.lutukai.pos.utils.getToken
import com.lutukai.pos.utils.isNetworkConnected
import com.lutukai.pos.utils.showToast
import kotlinx.android.synthetic.main.activity_asset_types.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class AssetTypes : AppCompatActivity() {
    private lateinit var generalViewModel: GeneralViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_asset_types)
        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        rv_asset_types.layoutManager = LinearLayoutManager(this)
        rv_asset_types.isNestedScrollingEnabled = true

//        generalViewModel = ViewModelProviders.of(this).get(GeneralViewModel::class.java)
        getAssetTypes()
    }

    private fun getAssetTypes() {
        if (isNetworkConnected(this)){
//            generalViewModel.getUpdatedAssetTypes()
            GlobalScope.launch(Dispatchers.Main) { assetTypes()  }
        }else{
            showToast("From Room")
        }
//        generalViewModel.getLocalAssetTypes().observe(this@AssetTypes, Observer {asstTypes ->
//            if (asstTypes.isNotEmpty()){
//                progress.visibility = View.GONE
//                var assetTypesAdapter = AssetTypesAdapter(this, asstTypes)
//                rv_asset_types.adapter = assetTypesAdapter
//            }
//
//        })
    }

    private suspend fun assetTypes() {
        try {
            val response = ApiClient.webService.fetchAssets(getToken())
            if (response.isSuccessful){
                val assets = response.body()!!.asset_types
                assets.let {
                    progress.visibility = View.GONE
                    val assetTypesAdapter = AssetTypesAdapter(this, assets)
                    rv_asset_types.adapter = assetTypesAdapter
                }
            }
        }catch (e:IOException){

        }

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
