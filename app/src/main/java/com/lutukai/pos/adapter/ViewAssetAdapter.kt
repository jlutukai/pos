package com.lutukai.pos.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.lutukai.pos.R
import com.lutukai.pos.activity.ViewAssets
import com.lutukai.pos.model.Assetss
import com.lutukai.pos.model.Message
import com.lutukai.pos.retrofit.ApiClient
import com.lutukai.pos.utils.getToken
import com.lutukai.pos.utils.showToast
import kotlinx.android.synthetic.main.item_asset_status.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class ViewAssetAdapter(val context: Context, private var assets: MutableList<Assetss>, private val statuses : List<String>, private val id : String,
                       private val lat : String, private val long : String) :
    RecyclerView.Adapter<ViewAssetAdapter.ViewAssetsViewModel>() {
    private val viewAssets : ViewAssets = ViewAssets()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewAssetAdapter.ViewAssetsViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_asset_status, parent, false)
        return ViewAssetsViewModel(v)
    }

    override fun getItemCount(): Int {
        return assets.size
    }

    override fun onBindViewHolder(holder: ViewAssetAdapter.ViewAssetsViewModel, position: Int) {
        val asset = assets[position]
        holder.setData(asset, position)
    }

    fun update(updatedassets: List<Assetss>){
        assets.clear()
        assets = updatedassets as MutableList<Assetss>
    }

    inner class ViewAssetsViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: Assetss? = null
        private var currentPos: Int = 0

        init {
            itemView.recieve.setOnClickListener {
                val s = "2"
                val aid = current!!.id.toString()
                changeStatus(s, aid)
            }
            itemView.reject.setOnClickListener {
                val s = "3"
                val aid = current!!.id.toString()
                changeStatus(s, aid)
            }
        }

        fun setData(asset: Assetss, position: Int) {
            asset.let {
                itemView.serial.text = asset.serial
                val s : String = statuses[asset.pivot.status]
                itemView.status.text = s
           if (asset.pivot.status == 1){
                    itemView.recieve.visibility = View.VISIBLE
                    itemView.reject.visibility = View.VISIBLE
                }
            current = asset
            currentPos = position
        }
        }

    }

     fun changeStatus(s: String, aid : String) {
        GlobalScope.launch(Dispatchers.Main) {
            if (change(s, aid)){
            context.startActivity(Intent(context, ViewAssets::class.java).putExtra("id", id))

            }
        }
    }

    private suspend fun change(s: String,aid : String) : Boolean{
        try {
            val response = ApiClient.webService.changeStatus(context.getToken(),id,aid, lat, long,s)
            if (response.isSuccessful){
                return true
            }else{
                val gson = Gson()
                val errorResponse = gson.fromJson<Message>(response.errorBody()!!.charStream(),
                    Message::class.java)
                val msg = errorResponse.message
                context.showToast(msg)
                return false
            }
        }catch (e:IOException){

        }
        return false
    }
}