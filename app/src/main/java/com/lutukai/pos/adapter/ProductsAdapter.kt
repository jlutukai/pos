package com.lutukai.pos.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lutukai.pos.R
import com.lutukai.pos.model.Product
import kotlinx.android.synthetic.main.item_product.view.*

class ProductsAdapter(private val context: Context, private val product: List<Product>) :
    RecyclerView.Adapter<ProductsAdapter.ProductsHolder>() {
    var onItemClick: ((Product) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_product, parent, false)
        return ProductsHolder(view)
    }

    override fun getItemCount(): Int = product.size

    override fun onBindViewHolder(holder: ProductsHolder, position: Int) {
        val prod = product[position]
        holder.setData(prod, position)
    }

    inner class ProductsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: Product? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper.setOnClickListener {
                current?.let {
                    onItemClick?.invoke(it)
                }
            }
        }


        fun setData(product: Product?, pos: Int) {
            product?.let {
                itemView.prod_name.text = product.name
                itemView.price_product.text = "KES. ${product.default_sell_price}"
            }
            current = product
            currentPos = pos
        }
    }
}