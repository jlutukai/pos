package com.lutukai.pos.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lutukai.pos.R
import com.lutukai.pos.activity.Assets
import com.lutukai.pos.model.AllBatches
import com.lutukai.pos.utils.showToast
import kotlinx.android.synthetic.main.item_contact_group.view.*

class BatchesAdapter (val context: Context, private val batches: List<AllBatches>) :
    RecyclerView.Adapter<BatchesAdapter.BatchesViewModel>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BatchesAdapter.BatchesViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_batch, parent, false)
        return BatchesViewModel(v)
    }

    override fun getItemCount(): Int {
        return batches.size
    }

    override fun onBindViewHolder(holder: BatchesAdapter.BatchesViewModel, position: Int) {
        val batch = batches[position]
        holder.setData(batch, position)
    }
    inner class BatchesViewModel(itemView: View) : RecyclerView.ViewHolder(itemView){
        private var current: AllBatches? = null
        private var currentPos: Int = 0
        init {
            itemView.main_wrapper.setOnClickListener {
                current.let {
                    val intent = Intent(context, Assets::class.java)
                    intent.putExtra("sid", current!!.received_at)
                    intent.putExtra("id", current!!.id.toString() )
                    context.showToast(current!!.received_at +" "+current!!.id.toString())
                    context.startActivity(intent)
                }
            }
        }

        fun setData(batch: AllBatches, position: Int) {
            batch.let {
                itemView.cust_group.text = batch.batch_no
            }
            current = batch
            currentPos = position
        }

    }
}