package com.lutukai.pos.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.l4digital.fastscroll.FastScroller
import com.lutukai.pos.R
import com.lutukai.pos.activity.Contacts
import com.lutukai.pos.activity.EditContact
import com.lutukai.pos.model.Contact
import kotlinx.android.synthetic.main.item_contact.view.*
import kotlinx.android.synthetic.main.item_contact_group.view.main_wrapper

class ContactAdapter(val context: Context, private val contacts: List<Contact>) :
    RecyclerView.Adapter<ContactAdapter.ContactViewModel>(), FastScroller.SectionIndexer {
    private val co: Contacts = Contacts()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ContactViewModel {
        val v = LayoutInflater.from(context).inflate(R.layout.item_contact, parent, false)
        return ContactViewModel(v)
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    @SuppressLint("DefaultLocale")
    override fun getSectionText(position: Int): CharSequence {
        return contacts[position].name[0].toString().capitalize()
    }

    override fun onBindViewHolder(holder: ContactViewModel, position: Int) {
        val contact = contacts[position]
        holder.setData(contact, position)
    }

    inner class ContactViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: Contact? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper.setOnClickListener {
                current.let {
                    val name: String = current!!.name
                    val id: String = current!!.id.toString()
                    val email: String? = current!!.email
                    val number: String = current!!.mobile
                    val biz: String? = current!!.supplier_business_name

                    val dialog = Dialog(context)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog.setCancelable(true)
                    dialog.setContentView(R.layout.contact_dialog)
                    val nameC: TextView = dialog.findViewById(R.id.name_contact)
                    nameC.text = name
                    val emailC: TextView = dialog.findViewById(R.id.email_contact)
                    if (!email.isNullOrBlank()) {
                        emailC.visibility = View.VISIBLE
                        emailC.text = email
                    }
                    val phone: TextView = dialog.findViewById(R.id.mobile_contacts)
                    phone.text = number
                    val business: TextView = dialog.findViewById(R.id.biz_contacts)
                    if (!biz.isNullOrBlank()) {
                        business.visibility = View.VISIBLE
                        business.text = biz
                    }
                    val close: ImageView = dialog.findViewById(R.id.close_dialog_contact)
                    close.setOnClickListener {
                        dialog.dismiss()
                    }
                    val edit: TextView = dialog.findViewById(R.id.edit_dialog_contact)
                    edit.setOnClickListener {
                        val intent = Intent(context, EditContact::class.java)
                        intent.putExtra("id", id)
                        intent.putExtra("name", name)
                        intent.putExtra("email", email)
                        intent.putExtra("number", number)
                        intent.putExtra("biz", biz)
                        context.startActivity(intent)
                        dialog.dismiss()
                    }
                    dialog.show()
                    val window = dialog.window
                    window!!.setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                }

            }
        }

        fun setData(contact: Contact, position: Int) {
            contact.let {
                itemView.name.text = contact.name
            }
            current = contact
            currentPos = position
        }

    }
}