package com.lutukai.pos.adapter


import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lutukai.pos.R
import com.lutukai.pos.activity.ProductsActivity
import com.lutukai.pos.model.Category
import kotlinx.android.synthetic.main.item_categories.view.*
import java.text.SimpleDateFormat
import java.util.*


class CategoriesAdapter(val context: Context, private val users: List<Category>) :
    RecyclerView.Adapter<CategoriesAdapter.CategoriesHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_categories, parent, false)
        return CategoriesHolder(view)
    }

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: CategoriesHolder, position: Int) {
        val user = users[position]
        holder.setData(user, position)
    }

    inner class CategoriesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: Category? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper.setOnClickListener {
                current?.let {
                    val intent = Intent(context, ProductsActivity::class.java)
                    intent.putExtra("id", current!!.id)

                    context.startActivity(intent)
                }
            }
        }

        fun setData(category: Category?, pos: Int) {
            category?.let {
                val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
//                val timestamp = category.updated_at.format(formatter)
                itemView.cat_name.text = category.name
                val date = formatter.parse(category.updated_at)
                itemView.date.setReferenceTime(date!!.time)
            }
            current = category
            currentPos = pos
        }
    }
}