package com.lutukai.pos.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.lutukai.pos.model.*

@Database(entities = [
    Category::class, Product::class,
    SalesLocal::class, ContactGroup::class,
    Contact::class
], version = 8)
abstract class AppDatabase : RoomDatabase() {
    abstract fun generalDao(): GeneralDao
}